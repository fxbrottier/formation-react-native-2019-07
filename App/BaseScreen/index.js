import React from "react";
import { StyleSheet, View } from "react-native";
import Constants from "expo-constants";
import styled from "styled-components/native";

const Wrapper = styled.View({
  paddingTop: Constants.statusBarHeight
});

export default function BaseScreen({ children }) {
  return <Wrapper>{children}</Wrapper>;
}
