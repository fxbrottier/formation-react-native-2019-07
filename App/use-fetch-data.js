import { useState, useEffect } from "react";

export default fetchHttClient => {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const jsonResult = await fetchHttClient();
      setData(jsonResult);
    }
    fetchData();
  }, [fetchHttClient]);

  return data;
};
