const apiBaseName = "https://opendata.bordeaux-metropole.fr";

export const fetchEvents = async () => {
  try {
    const response = await fetch(
      `${apiBaseName}/api/records/1.0/search/?dataset=met_agenda&facet=location_city&facet=origin_title`
    );
    const result = await response.json();

    return result.records;
  } catch (err) {
    console.error(err);
  }
};
