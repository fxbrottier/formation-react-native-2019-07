import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import React from "react";

import { Screen as EventsScreen, Icon as EventsIcon } from "../../Events";
import { Screen as HomeScreen, Icon as HomeIcon } from "../../Home";
import { Screen as SettingsScreen, Icon as SettingsIcon } from "../../Settings";

const activeColor = "#f0edf6";
const focusedColor = "#8b76c1";

export default createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <HomeIcon color={focused ? activeColor : focusedColor} />
        )
      }
    },
    Events: {
      screen: EventsScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <EventsIcon color={focused ? activeColor : focusedColor} />
        )
      }
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <SettingsIcon color={focused ? activeColor : focusedColor} />
        )
      }
    }
  },
  {
    initialRouteName: "Home",
    activeColor,
    inactiveColor: "#3e2465",
    barStyle: { backgroundColor: "#694fad" }
  }
);
