import React from "react";
import { Title } from "react-native-paper";
import styled from "styled-components/native";

import BaseScreen from "../../App/BaseScreen";

const CenterBaseScreen = styled(BaseScreen)({
  justifyContent: "center",
  alignItems: "center"
});

export default function HomeScreen() {
  return (
    <CenterBaseScreen>
      <Title>Bdx Events</Title>
    </CenterBaseScreen>
  );
}
