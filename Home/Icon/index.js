import React from "react";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

export default props => <MaterialIcons name="home" size={25} {...props} />;
