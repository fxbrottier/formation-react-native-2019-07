import React from "react";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

export default props => <MaterialIcons name="build" size={25} {...props} />;
