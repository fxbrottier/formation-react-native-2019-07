import React from "react";
import { Title } from "react-native-paper";

import BaseScreen from "../../App/BaseScreen";
import Version from "../Version";

export default function SettingsScreen() {
  return (
    <BaseScreen>
      <Title>SettingsScreen</Title>
      <Version />
    </BaseScreen>
  );
}
