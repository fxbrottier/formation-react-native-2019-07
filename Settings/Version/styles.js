import styled from "styled-components/native";
import { Title } from "react-native-paper";

export const VersionOS = styled(Title)(({ platform }) => ({
  color: platform === "android" ? "green" : "blue"
}));
