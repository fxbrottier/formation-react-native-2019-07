import React from "react";
import { Title } from "react-native-paper";
import { Platform } from "react-native";

import { VersionOS } from "./styles";

export default function Version() {
  return (
    <>
      <VersionOS platform={Platform.OS}>{Platform.OS}</VersionOS>
      <Title>{Platform.Version}</Title>
    </>
  );
}
