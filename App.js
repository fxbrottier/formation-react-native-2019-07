import React from "react";
import { Provider as PaperProvider } from "react-native-paper";
import { createAppContainer } from "react-navigation";

import TabNavigation from "./App/TabNavigation";

const MainNavigation = createAppContainer(TabNavigation);

export default function App() {
  return (
    <PaperProvider>
      <MainNavigation />
    </PaperProvider>
  );
}
