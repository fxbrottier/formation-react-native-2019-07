# Events BDX

## Getting Started

Install all dependencies:

```shell
$ yarn
```

Run the app:

```shell
$ yarn start
```

Enjoy!
