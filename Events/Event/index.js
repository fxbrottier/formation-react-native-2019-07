import { Avatar, Button, Card, Paragraph } from "react-native-paper";
import { Linking } from "react-native";
import { shape, string } from "prop-types";
import React from "react";

function EventActions({ registrationurl }) {
  if (!registrationurl) return null;

  const openRegistrationUrl = () => {
    Linking.openURL(registrationurl);
  };

  return (
    <Card.Actions>
      <Button onPress={openRegistrationUrl}>Réservation</Button>
    </Card.Actions>
  );
}

export default function Event({ fields }) {
  const { title_fr, location_name, registrationurl, uid } = fields;

  return (
    <Card>
      <Card.Title
        title={title_fr}
        subtitle={location_name}
        left={props => <Avatar.Icon {...props} icon="folder" />}
      />
      <Card.Content>
        <Paragraph>Card content</Paragraph>
      </Card.Content>
      <Card.Cover source={{ uri: `https://picsum.photos/700?random=${uid}` }} />
      <EventActions registrationurl={registrationurl} />
    </Card>
  );
}

Event.propTypes = {
  fields: shape({
    title_fr: string,
    location_name: string,
    registrationurl: string,
    uid: string
  })
};
