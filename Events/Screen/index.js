import { ActivityIndicator } from "react-native-paper";
import { FlatList } from "react-native";
import React from "react";

import BaseScreen from "../../App/BaseScreen";
import Event from "../Event";
import useFetchData from "../../App/use-fetch-data";
import { fetchEvents } from "../../App/open-data-http-client";

export default function EventsScreen() {
  const events = useFetchData(fetchEvents);

  const hasEvents = events.length > 0;

  return (
    <BaseScreen>
      {!hasEvents && <ActivityIndicator />}
      <FlatList
        data={events}
        renderItem={({ item }) => <Event {...item} />}
        keyExtractor={(item, index) => index.toString()}
      />
    </BaseScreen>
  );
}
